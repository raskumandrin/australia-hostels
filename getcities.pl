#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;


my %states = (
	'New-South-Wales'              => 'http://www.hostelz.com/hostels-in/Australia/New-South-Wales',
	'South-Australia'              => 'http://www.hostelz.com/hostels-in/Australia/South-Australia',
	'Queensland'                   => 'http://www.hostelz.com/hostels-in/Australia/Queensland',
	'Western-Australia'            => 'http://www.hostelz.com/hostels-in/Australia/Western-Australia',
	'Northern-Territory'           => 'http://www.hostelz.com/hostels-in/Australia/Northern-Territory',
	'Victoria'                     => 'http://www.hostelz.com/hostels-in/Australia/Victoria',
	'Tasmania'                     => 'http://www.hostelz.com/hostels-in/Australia/Tasmania',
	'Australian-Capital-Territory' => 'http://www.hostelz.com/hostels-in/Australia/Australian-Capital-Territory',
);


foreach my $state (keys %states) {
	`mkdir -p data/$state`;

#	$ua->get($states{$state})->res->content->asset->move_to("data/$state.html");

	Mojo::DOM->new( $ua->get($states{$state})->res->body )->find('a')->each(sub {
		my $a = shift;
#		say $a->text;

		if (defined $a->{href} and $a->{href} =~ qr($state\/) and $a->{href} =~ qr(hostels) ) {
#			say $a->{href};

#			/hostels/Australia/Tasmania/Bicheno
#			/hostels/Australia/Tasmania/Bridport
#			/hostels/Australia/Tasmania/Coles-Bay
#			/hostels/Australia/Tasmania/Devonport
#			/hostels/Australia/Tasmania/Eaglehawk-Neck

			my $city_link = $a->{href};
			my ($city) = ( $city_link =~ /\/([^\/]+)$/ );
#			say $city;

			$ua->get( "http://www.hostelz.com$city_link" )->res->content->asset->move_to("data/$state/$city.html");

		}
#		say $a->text;
		
	});

}