#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

local $| = 1;

use feature 'say';

my $ua = Mojo::UserAgent->new;


my %states = (
	'New-South-Wales'              => 'http://www.hostelz.com/hostels-in/Australia/New-South-Wales',
	'South-Australia'              => 'http://www.hostelz.com/hostels-in/Australia/South-Australia',
	'Queensland'                   => 'http://www.hostelz.com/hostels-in/Australia/Queensland',
	'Western-Australia'            => 'http://www.hostelz.com/hostels-in/Australia/Western-Australia',
	'Northern-Territory'           => 'http://www.hostelz.com/hostels-in/Australia/Northern-Territory',
	'Victoria'                     => 'http://www.hostelz.com/hostels-in/Australia/Victoria',
	'Tasmania'                     => 'http://www.hostelz.com/hostels-in/Australia/Tasmania',
	'Australian-Capital-Territory' => 'http://www.hostelz.com/hostels-in/Australia/Australian-Capital-Territory',
);

my %state_titles = (
	'New-South-Wales'              => 'New South Wales',
	'South-Australia'              => 'South Australia',
	'Queensland'                   => 'Queensland',
	'Western-Australia'            => 'Western Australia',
	'Northern-Territory'           => 'Northern Territory',
	'Victoria'                     => 'Victoria',
	'Tasmania'                     => 'Tasmania',
	'Australian-Capital-Territory' => 'Australian Capital Territory',
);

my %city_links;

foreach my $state (keys %states) {
	`mkdir -p data/$state`;

#	$ua->get($states{$state})->res->content->asset->move_to("data/$state.html");

	Mojo::DOM->new( $ua->get($states{$state})->res->body )->find('a')->each(sub {
		my $a = shift;
#		say $a->text;


		if (defined $a->{href} and $a->{href} =~ qr($state\/) and $a->{href} =~ qr(hostels) ) {
#			say $a->{href};

#			/hostels/Australia/Tasmania/Bicheno
#			/hostels/Australia/Tasmania/Bridport
#			/hostels/Australia/Tasmania/Coles-Bay
#			/hostels/Australia/Tasmania/Devonport
#			/hostels/Australia/Tasmania/Eaglehawk-Neck

			my $city_link = $a->{href};
			my $city_title = $a->text;
			
			$city_links{$city_link} ++;
			
			if ( $city_links{$city_link} == 1 ) {
			
				my ($city) = ( $city_link =~ /\/([^\/]+)$/ );
	#			say $city;

				$ua->get( "http://www.hostelz.com$city_link" )->res->content->asset->move_to("data/$state/$city.html");

				Mojo::DOM->new( $ua->get( "http://www.hostelz.com$city_link" )->res->body )->find('a')->each(sub {
					my $hostel = shift;
	#				say $hostel->text;
					if (defined $hostel->{href} and $hostel->{href} =~ qr(^\/hostel\/) and $hostel->text) {
						my $hostel_link = $hostel->{href};
						my $hostel_title = $hostel->text;
						my $hostel_website;


						Mojo::DOM->new( $ua->get( "http://www.hostelz.com$hostel_link" )->res->body )->find('a')->each(sub {
							my $website = shift;

							if (defined $website->{href} and $website->{href} =~ qr(^\/redir\.php\?id=) and $website->text) {
								$hostel_website = $website->text;
							}
						});
						$hostel_website = "http://$hostel_website" if $hostel_website;

						say "$state_titles{$state}\t$city_title\t$hostel_title\t$hostel_website\thttp://www.hostelz.com$hostel_link";

					}
				});
			
			} # if ( $city_links{$city_link} == 1 )
		}
#		say $a->text;
		
	});

}